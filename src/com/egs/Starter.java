package com.egs;

import com.egs.customexceptions.MandatoryFieldException;
import com.egs.model.Atm;
import com.egs.model.Card;
import com.egs.model.Person;
import com.egs.service.InitService;

public class Starter {

    public static void main(String[] args) {


        InitService initService = new InitService();
        try {
            initService.initialize();
        } catch (MandatoryFieldException e) {
            e.printStackTrace();
        }

        Atm atm = new Atm();


        Thread firstThread = new Thread(() -> {

            Person firstPerson = initService.getRandomPerson();

            Card theCard = null;

            try {
                 theCard = firstPerson.getRandomCard();
            } catch (MandatoryFieldException e) {
                e.printStackTrace();
            }
            atm.getCash(theCard,2000);
        });


        Thread secondThread = new Thread(() -> {

            Person secondPerson = initService.getRandomPerson();

            Card theCard = null;

                try {
                theCard = secondPerson.getRandomCard();
            } catch (MandatoryFieldException e) {
                e.printStackTrace();
            }
                atm.getCash(theCard,1200);
        });

        firstThread.setName("Bob");
        secondThread.setName("Jack");
        secondThread.start();
        firstThread.start();


    }
}
